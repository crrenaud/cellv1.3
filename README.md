# CellV1.3



## Name
(toto) cell V1.3

## Description
cell is a command line application that allows to analyse a text file that describes the evolution of a cells population during a video-microscopy experiment. It provides as outputs (i) an image in the SVG format that allows to more easily understand the effect of some medical treatment and (ii) some statistical analysis about this evolution in a CSV file.



## Installation
cell is provided with two binaries file that should run either under the Linux Operating System (bin/cell) and Windows Operating System (bin/wcell.exe). According to the target OS, the corresponding binary file can be copied in the directory in which the software should be used.

If you want to recompile the software under Linux, you just have to copy the Src folder and the CMakeLists.txt file into the destination folder on your machine, to open a system console, to move in the destination folder and then to type the following commands :

cmake .

make

The binary file cell will be generated in the ./bin directory.

## Usage
for any information on how to use this software please read the documentation that is provided in the Doc directory.




## License
  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.
  

