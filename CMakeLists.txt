cmake_minimum_required (VERSION 3.0)
project (cell)
set(SRCS
  ./Src/cell.cpp
  ./Src/Node.cpp
  ./Src/Input.cpp
  ./Src/SvgExporter.cpp
  ./Src/WaitingSet.cpp
  ./Src/Stat.cpp
  )
    
set(HEADERS
  ./Src/Node.hpp
  ./Src/Input.hpp
  ./Src/SvgExporter.hpp
  ./Src/WaitingSet.hpp
  ./Src/Stat.hpp
)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ./bin)

add_definitions(-std=c++11)

add_executable(cell ${SRCS} ${HEADERS})




