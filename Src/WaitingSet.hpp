/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WAITINGSET
#define WAITINGSET

#include <iostream>
#include <vector>
using namespace std;

#include "Node.hpp"


/**
 * this structure stores some information about the drawinf area of a cell
 */
struct location{
  int beg;// beginning of the vertical drawing area of a cell
  int width;// width of the vertical drawing area of a cell
  int t;// time stamp of the cell
};

/**
 * \class WaitingElement
 * This clas is used to represent a node that must wait for some additional
 * information before it can be drawn. This is required when
 * the node is the son of several fusionned cells.
 */
class WaitingElement {
public:
  Node *pcell; // pointer to the waiting fusioned cell
  vector <location> pos; // drawing locations of parent's cells
  int nbParentsEncountered;// count the number of parents that have yet be encountered
public:
  WaitingElement();
  WaitingElement(Node *pcell, int b, int w, int t);
  Node* getCell(){ return pcell;}
  bool isready(){ return nbParentsEncountered==pcell->getNbParents();}

  /**
   * This function allows to draw the SVG elements that represent the fusion
   * of several cells when all the parents of a fusioned cell have been
   * encoutered.
   * @param exporter the object that allows to save de drawing instruction
   * in the SVG format
   * @param radius the radius of the circle that represents a cell in SVG
   */
  void tosvg(SvgExporter &exporter, int radius);
  
  friend ostream& operator<<(ostream& out, const WaitingElement &e);

};

/**
 * \class WaitingSet
 * This class is used to manage a pending set of nodes waitng for collecting
 * all their parent nodes for a fusion operation.
 */
class WaitingSet {
private:
  vector <WaitingElement> elt;// the list of waitng "nodes"
public:
  WaitingSet(){};

  ~WaitingSet(){ elt.clear();}

  /**
   * add a new waiting "node"
   * @param e the waiting element to be added
   * @return  -1 if the added element cannot be used yet 
   * (in which case it is added to the waiting list), and its index
   * in the list if it can be used.
   */
  int add(WaitingElement e);

  /**
   * Access the ith element in the waiting list.
   * @param i the index of the element in the waiting list.
   * i must be a valid index.
   * @return le ith eelment of the waiting list.
   */
  WaitingElement operator[](int i){
    return elt[i];
  }

};
#endif
