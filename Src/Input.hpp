/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUT_HPP
#define INPUT_HPP

#include <iostream>
#include <stdexcept>
using namespace std;

// verify whether a name exists in the table of names
// @return : -1 if name does not exist, its location otherwise
extern bool exist(const string &name);

// Read the cells events file.
// @return false if any error occurs, true otherwise
extern bool read(const string & filename, const string &logfile);




#endif
