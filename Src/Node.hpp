/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "SvgExporter.hpp"
#include "Stat.hpp"

// Values of the different available cell states
enum State { UNKNOWN, ALIVE, END, DEAD, OUT, DIV, FUSION};

class Node;
class Statistics;

/**
 * \class Name
 * This class associates a celle name to its memory data structure.
 */
class Name {
private:
  string name;/**< cell's name */
  Node *pcell;/**< pointer to the cell's data structure */
  
public:
  Name(){ pcell = nullptr; }
  Name(const string &n, Node *c) : name(n), pcell(c){}

  const string& getName(){ return name; }
  Node *getNode(){ return pcell; }
  
  friend ostream& operator<<(ostream& out, const Name &n);
};

/**
 * \class Node
 * This class allows to represent one statz of a cell's life.
 */
class Node {
private:
  int id; // indice of the name in names
  int time; // time stamp of the node
  int end; // final time stamp of the node
  //int width; // width of display area (in pixels)
  State state; // cell's state
  int sons; // number of sons
  int parents; // number of parents (mainly for fusioned node)
  Node **tabSons; // array of cell's sons (in case of subdivision)
public:
  Node();
  Node(int id, int time);

  void setEnd(State s, int t){
    end = t; state = s;
  }

  int getEnd(){ return end;}
  int getStart(){ return time;}

  State getState(){ return state; }
  int getNbParents(){ return parents; }

  /** 
   * This function allows to create the son's array of the current node when subdivision occurs.
   * @param nbdiv the number of son's cells during subdivision.
   */
  void createSons(int nbdiv);

  /**
   * Add a node representing a cell to the current node 's array of sons.
   * @param pcell a pointer to the new node that should be added to the current
   *  node's array of sons
   * @return true if the node has been correctly added, false if the sons's array
   * is out of space.
   */
  bool addSon(Node* pcell);

  /**
   * return the width of the node according to its number of sons.
   * @param curwidth is the current width before calling the method. This
   * parameter is required in order to compute the correct initial node width
   * when its cell's tree hase some fusion events.
   * @return 1 if the node has no sons, the number of sons otherwise.
   */
  int width(int curwidth);

  /**
   * Recursively collect statistics in the current node subtree.
   * @param stat is used to store the different statistics for the subtree
   * @param divActive has value true if the function is called after a cell's subdivision,
   * false otherwise
   * @param curDivTime is used in oder to know the current subdivision time for collecting
   * statistics during subdivisions.
   */
  void collectStatistics(Statistics &stat, bool divActive, int curDivTime);

  /**
   * Recursively export the cell's subtree to the SVG drawing format.
   * @param exporter is the class in charge of drawing the node subtree to th SVG format
   * @param beg is the x-location of the area used for drawing the node subtree
   * @param width is the width (in pixels) of the area used for drawing the node subtree
   */
  void tosvg(SvgExporter &exporter, int beg, int width);

  friend ostream& operator<<(ostream& out, const Node &n);
};

// Data structures required for handling the drawing of fusion events
struct Position {
  int x; // x-axis location of node
  int Td; // Time stamp of the node
  int Tf; // Final time stamp of the node
};

struct InfoFusion {
  Node *pcell;
  vector <Position> pos;
};

#endif
