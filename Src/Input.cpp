/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "Node.hpp"

class readException : public exception {
private:
  string mess;
public:
  readException(const string &s){ mess = s; }
  string what()  { return mess;}
};


/**
 * Reading the analysis data of a cell evolution sequence
 * the reading format is as follows
 * T begin name : start of a cell with name "name" at time T
 * T end name : end of a cell with name "name" at time T
 * T out name : exit of a cell with name "name" at time T
 * T death name : death of a cell with name "name" at time T
 * T div name N name1 name2 ... nameN : division of a cell with name "name" into N cells
 *  with names "name1", "name2", ... "nameN"  at time T
 * T fusion name1 ... nameP in name : merge the cells "name1", ...
 * "nameP" into one cell named "name" at time T
 */

  
extern vector <Name> names; // vector of cell's names
extern vector <Node *> cells; //  vector of initial cell's trees
extern string color; // drawing color

// declaring predefined functions
void begin(ifstream &in, int time, bool log, ofstream &out);
void end(ifstream &in, int time, bool log, ofstream &out);
void out(ifstream &in, int time, bool log, ofstream &output);
void death(ifstream &in, int time, bool log, ofstream &out);
void div(ifstream &in, int time, bool log, ofstream &out);
void fusion(ifstream &in, int time, bool log, ofstream &out);

/**
 *  verify whether a name exists in the table of names
 * @return : -1 if name does not exist, its location otherwise
 */
int exist(const string &name){
  for(int i=0; i< names.size(); i++)
    if(name == names[i].getName()) return i;
  return -1;
}

/**
 * Read the cells events file.
 * @param filename the name of the event file
 * @param logfilename the name of the log file. This on wiil be written with the output of all reading.
 * @return false if any error occurs, true otherwise
 */
bool read(const string & filename, const string &logfilename){
  // Opening data file
  ifstream in(filename);
  if(!in.is_open()){
    cerr << "Error opening " << filename << endl;
    return false;
  }

  // opening log file
  bool log=true;
  ofstream output(logfilename);
  if(!output.is_open()){
    cerr << "Error opening log file " << filename << endl;
    log=false;
  }
  // reading the entire file of events
  
  string stime; // event's time
  in >> stime;
  
  while(!in.eof()){// reading an event
    
    // detection of a comment line
    if(stime[0]=='#'){
      getline(in, stime);
      in >> stime;
      continue;
    }

    if(stime=="color"){
      in >> color;
      in >> stime;
      if(log) output << "color = " << color << endl;
      continue;
    }

    // decoding the time stamp in stime
    int time = atoi(stime.c_str());

    // retrieve the event
    string event; // type of event
    in>> event;
    
    // event type analysis
    try {
      if(event=="begin"){// new cell
	begin(in, time, log, output);
       }else if(event=="end"){// end of image sequence for the cell
	end(in, time, log, output);
      }else if(event=="death"){// death of cell
	death(in, time, log, output);
      }else if(event=="out"){// output of cell
	out(in, time, log, output);
      }else if(event=="div"){// division of cell
	div(in, time, log, output);
      }else  if(event=="fusion"){// fusion of cells
	fusion(in, time, log, output);
      } else {// unknown event
	stringstream err;
	err << "unknown event : " << event;
	throw readException(err.str());
      }
    } catch (readException &e){
      cout << "Error - " << e.what() << endl;
      names.clear();
      cells.clear();
      in.close();
      if(log) output.close();
      return false;
    }
    
    // reading next event
    in >> stime;
  }

  // reading was ok
  in.close();
  if(log) output.close();
  return true;
}


/**
 * This function ensures the execution of all the operations required when the begin keyword
 * is encountered in the cell event file.
 * @param in is the input filestream of the events
 * @param time is the date the "begin" event occurs
 * @param log if true allows the output in the log file
 * @param out is the output filestreams of the log file
 */
void begin(ifstream &in, int time, bool log, ofstream &out){
  string name;
  in >> name;
  if(exist(name)>=0){
    stringstream err;
    err << "cell " << name << " still defined ... cannot use begin" << endl;
    throw readException(err.str());
  } else {
    if(log) out << "cell " << name << " created" << endl;
    Node *ncell = new Node(names.size(), time);
    Name nname(name, ncell);
    names.push_back(nname);
    cells.push_back(ncell);
  }
}


/**
 * This function ensures the execution of all the operations required when the end keyword
 * is encountered in the cell event file.
 * @param in is the input filestream of the events
 * @param time is the date the "end" event occurs
 * @param log if true allows the output in the log file
 * @param out is the output filestreams of the log file
 */
void end(ifstream &in, int time, bool log, ofstream &out){
  string name;
  in >> name;
  int loc = exist(name);
  if(loc>=0){
    names[loc].getNode()->setEnd(END, time);
    if(log) out << "cell " <<name << " end of sequence" << endl;
  } else {
    stringstream err;
    err << "end : cell " << name << " undefined... ";
    throw readException(err.str());
  }
}

/**
 * This function ensures the execution of all the operations required when the out keyword
 * is encountered in the cell event file.
 * @param in is the input filestream of the events
 * @param time is the date the "out" event occurs
 * @param log if true allows the output in the log file
 * @param out is the output filestreams of the log file
 */
void out(ifstream &in, int time, bool log, ofstream &output){
  string name;
  in >> name;
  int loc = exist(name);
  if(loc>=0){
    names[loc].getNode()->setEnd(OUT, time);
    if(log) output << "cell " << name << " out of image at  " << time << endl;
  } else {
    stringstream err;
    err << "out : cell " << name << " undefined... ";
    throw readException(err.str());
  }
}

/**
 * This function ensures the execution of all the operations required when the death keyword
 * is encountered in the cell event file.
 * @param in is the input filestream of the events
 * @param time is the date the "death" event occurs
 * @param log if true allows the output in the log file
 * @param out is the output filestreams of the log file
 */
void death(ifstream &in, int time, bool log, ofstream &out){
  string name;
  in >> name;
  int loc = exist(name);
  if(loc>=0){
    names[loc].getNode()->setEnd(DEAD, time);
    if(log) out << "cell " << name << " is dead at time " << time << endl;
  } else {
    stringstream err;
    err << "death : cell " << name << " undefined... ";
    throw readException(err.str());
  }
}

/**
 * This function ensures the execution of all the operations required when the div keyword
 * is encountered in the cell event file. The syntax of this keyword is: T div Name N name1 name2 ... nameN
 * @param in is the input filestream of the events
 * @param time is the date the "div" event occurs
 * @param log if true allows the output in the log file
 * @param out is the output filestreams of the log file
 */
void div(ifstream &in, int time, bool log, ofstream &out){
  string name;
  in >> name;
  int icell = exist(name);
  if(icell>=0){
    int nbdiv;
    in >> nbdiv;
    if(log) out << "cell " << name << " divides in " << nbdiv <<  " cells at time " << time << endl;
    // recovery of the cell
    Node *dcell = names[icell].getNode();
    dcell->setEnd(DIV, time);
    dcell->createSons(nbdiv);
    for(int i=0; i<nbdiv; i++){
      in >> name;
      if(exist(name)>=0){
	stringstream err;
	err << "division : name " << name << " still exists...";
	throw readException(err.str());
	return;
      }
      Node *ncell = new Node(names.size(), time);
      Name nname(name, ncell);
      names.push_back(nname);
      if(dcell->addSon(ncell)){
	if(log) out << "div : cell named " << name << " created" << endl;
      } else {
	stringstream err;
	err << "div : error when adding son " << name;
	throw readException(err.str());
	return;
      }
    }
  } else {
    stringstream err;
    err << "div : cell name " << name << " undefined";
    throw readException(err.str());
  }
}

/**
 * This function ensures the execution of all the operations required when the fusion keyword
 * is encountered in the cell event file. 
 * @param in is the input filestream of the events
 * @param time is the date the "fusion" event occurs
 * @param log if true allows the output in the log file
 * @param out is the output filestreams of the log file
 */
void fusion(ifstream &in, int time, bool log, ofstream &out){
  vector <int> id;
  // recovery of the cells to be merged
  string name;
  in >> name;
  while(name!="in"){
    int cellid=exist(name);
    if(cellid<0){
      stringstream err;
      err << "error : cell " << name << " undefined !!!";
      throw readException(err.str());
      return;
    }
    // the cell exists
    if(log) out << "cell " << name << " found" << endl;
    id.push_back(cellid);
    // recovery of the next cell
    in >> name;
  }// while

  // retrieve the name of the merged cell
  in >> name;
  if(exist(name)>=0){
    stringstream err;
    err << "error : cell " << name << " still exists...";
    throw readException(err.str());
    return;
  }

  // apply the merge 
  //input: we have the id of the cells to merge
  //create the merged cell
  Node *ncell = new Node(names.size(), time);
  Name nname(name, ncell);
  names.push_back(nname);

  // update the merged cells
  for(int i=0; i<id.size(); i++){
    Node *pcell = names[id[i]].getNode();
    pcell->setEnd(FUSION, time);
    pcell->createSons(1);
    pcell->addSon(ncell);
  }
}
