/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.hpp"

#include <vector>
#include <string>
#include <queue>
using namespace std;
extern vector <Name> names;

#include "WaitingSet.hpp"


// variable used to put some data on hold before it can actually be processed
WaitingSet listeFusion;
WaitingStatList listeStat;

Node::Node(){
  id = -1;
  time = end = -1;
  state = UNKNOWN;
  sons = 0;
  parents = 0;
  tabSons=nullptr;
}


Node::Node(int id, int time): id(id), time(time) {
  end = -1;
  state = ALIVE;
  sons = 0;
  parents = 0;
  tabSons=nullptr;
}

void Node::createSons(int nbDiv){
  sons = nbDiv;
  tabSons = new Node*[nbDiv];
  for(int i=0; i<nbDiv; i++) tabSons[i]=nullptr;
}

bool Node::addSon(Node *pcell){
  for(int i=0; i<sons; i++)
    if(tabSons[i]==nullptr){
      tabSons[i]=pcell;
      pcell->parents++;
      return true;;
    }
  return false;
}

  
int Node::width(int curwidth){
  if(sons==0) return 1;
  // return the sum of the widths of each son
  if(sons>1){ // division case
    int sum=0;// do not count the node that has sons
    for(int i=0; i<sons; i++)
      sum += tabSons[i]->width(curwidth);
    return sum;
  }
  // sons == 1 fusion
  int w = tabSons[0]->width(curwidth);
  if(w>curwidth) return w;
  return curwidth;
}


void Node::tosvg(SvgExporter &exporter, int beg, int width){
  // begining of the cell
  int radius=4;
  int mup = exporter.margin(UP);

  // cell's name if beginning in 0
  if(time==0){
    exporter.text(beg+width/2, mup/2, mup/2, names[id].getName());
  }
  // cell's begin
  exporter.circle(beg+width/2, time+mup, radius, "white");

  // cell's life
  if(state!=FUSION)
    exporter.line(beg+width/2, time+mup+radius, beg+width/2, end+mup, "black");

  switch(state){
  case END:
    exporter.circle(beg+width/2, end+mup, radius, "white");
    exporter.text(beg+width/2, end+mup+3*radius, 2*radius, to_string(end));
    break;
  case DEAD:
    exporter.cross(beg+width/2, end+mup, 2*radius, "red");
    exporter.text(beg+width/2, end+mup+3*radius, 2*radius, to_string(end));
    break;
  case OUT:
    exporter.parallel(beg+width/2, end+mup, 2*radius, "blue");
    exporter.text(beg+width/2, end+mup+3*radius, 2*radius, to_string(end));
    break;
  case DIV: {
    // draw the horizontal dividing line
    int wson = width / sons;// width for each son
    exporter.line(beg+wson/2, end+mup,
		  beg+width-wson/2, end+mup, "black");
    // indicate the time of division
    exporter.text(beg+width/2+wson/4, end+mup-2*radius, 2*radius, to_string(end));
    // handling each son
    for(int i=0; i<sons; i++)
      tabSons[i]->tosvg(exporter, beg+i*wson, wson);
    break;
  }// DIV
  case FUSION:{
    // We are on a FUSION cell which points to the merged child cell
    // We try to add the merged cell to the waiting list
    WaitingElement cur(this->tabSons[0],  beg, width, time);
    int id = listeFusion.add(cur);
    if(id>=0) listeFusion[id].tosvg(exporter, radius);
    break;
  }// FUSION

  }//switch
    
  
}

void Node::collectStatistics(Statistics &stat, bool divActive, int curDivTime){
  

    
  if(time==0){
    stat.nbInitCell++;
    // update data for first division statistics
    if(state==DIV){
      // assume that first div occurs only from initial cell
      stat.nbFirstDiv++;
      stat.sumTimeFirstDiv += end;
    }
    // add a new counter for div per cell
    stat.divpercell.push_back(0);
    stat.idcellname.push_back(id);// id in the name's vector
    // add a new counter for nb of final living cells per initial cell
    stat.nbLivingCell.push_back(0);
  }
  
  // if(state==END && end==721){
  //   stat.nbFinalCell++;
  //   stat.nbLivingCell[stat.nbLivingCell.size()-1]++;
  //   return;
  // }
  
  if(state==END){
    stat.nbLivingCell[stat.nbLivingCell.size()-1]++;
    if(end==721)
      stat.nbFinalCell++;
    return;
  }

  
  if(state==DEAD){
    stat.nbDeath++;
    if(divActive)
      stat.nbDeathWithDiv++;
    else
      stat.nbDeathWithoutDiv++;
    // V1.1.3
    int day=(end/144);// day of death
    if(day==5) day=4;// correction for time=721
    stat.nbDeathPerDay[day]++;
    return;
  }

  if(state==OUT){
    stat.nbOut++;
    return;
  }
  
  if(state==DIV){
    // V1.1.3
    int day=(end/144);// day of mitosis
    if(day==5) day=4;// correction for time=721
    stat.nbMitosisPerDay[day]++;
    // -----
    
    stat.nbTotalMitosis++;
 

    if(sons==2){// vérifier s'il n'y a pas une fusion juste après
      if(tabSons[0]->state!=FUSION && tabSons[1]->state!=FUSION)
	stat.nbNormalDiv++;
      else{
	stat.nbAbnormalDiv++;
	stat.nbAbnormalDivPerDay[day]++;
      }
      // divNormale=true;// à vérifier si on arrive d'une division anormale
    } else { // sons >2
      stat.nbNonDichotimicMitosis++;
      stat.nbAbnormalDiv++;
      stat.nbNonDichotimicMitosisPerDay[day]++;
      stat.nbAbnormalDivPerDay[day]++;
      // divNormale = false;
    }
    // update time between two div
    if(curDivTime>=0){
      stat.sumTimeBetweenDiv += (end-curDivTime);
      stat.nbIntervalBetweenDiv++;
    }
    curDivTime=end;

    // update number of div per cell
    stat.divpercell[stat.divpercell.size()-1]++;
    
    // collect statistics from sons
    for(int i=0; i<sons; i++)
      tabSons[i]->collectStatistics(stat, true, curDivTime);
    return;
  }

  if(state==FUSION){
    WaitingStat cur(this->tabSons[0]);
    int id = listeStat.add(cur);
    if(id>=0){
      stat.nbFusions++;
      // V1.1.3
      int day=(end/144);// day of fusion
      if(day==5) day=4;// correction for time=721
      stat.nbFusionPerDay[day]++;
      // ----
      listeStat[id].getCell()->collectStatistics(stat, divActive, curDivTime);
    }
  }
  


}

ostream& operator<<(ostream& out, const Node &n){
  out << n.time << " to " << n.end << " ";
  switch(n.state){
  case ALIVE: out << "alive "; break;
  case DEAD: out << "dead "; break;
  case OUT: out << "out "; break;
  case END : out << "end "; break;
  default: out << "unknown "; break;
  }
  out << n.sons << " sons" << endl;

  return out;
}


ostream& operator<<(ostream& out, const Name &n){
  out << "Cell " << n.name << " : " << endl;
  out << *(n.pcell) << endl;
  return out;
}
