/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SVG_EXPORTER_HPP
#define _SVG_EXPORTER_HPP

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// names of the margins location
enum Margin { UP, BOTTOM, LEFT, RIGHT };

/**
 * \class SvgExporter
 * This class takes in charge all the drawing operation when exporting
 * cell's node to an image using the SVG format/
 */
class SvgExporter {
private:
  ofstream out;
  int pageWidth, pageHeight;// total dimensions
  int mup, mbottom, mleft, mright;// margins
  int nbDays; // duration of the videomicroscopy experiment
  int nbImages; // total number of images captured during  the videomicroscopy experiment

  // definition of default dimensions
  static const int MARGIN_UP = 15;
  static const int MARGIN_BOTTOM = 15;
  static const int MARGIN_LEFT = 25;
  static const int MARGIN_RIGHT = 25;
  static const int NB_IMAGES = 720;
  static const int NB_DAYS = 5;
  static const int DEFAULT_WIDTH = 500+MARGIN_LEFT+MARGIN_RIGHT;
  static const int DEFAULT_HEIGHT = NB_IMAGES+MARGIN_UP+MARGIN_BOTTOM;
  
public:
  SvgExporter();
  ~SvgExporter();

  /**
   * Open the SVG output file and intialize the header of the SVG format.
   * @param filename the name of the output file which should have the .svg extension
   * @param generator the current name of the cell software
   * @param w the desired width of the drawing area for each initial cell (in pixels)
   * @param im the number of images to consider wehn exporting to SVG
   * @param days the length of the experiment in days
   * @param mup the size (in pixels) of the top margin
   * @param mbottom the size (in pixels) of the bottom margin
   * @param mleft the size (in pixels) of the left margin
   * @param mright the size (in pixels) of the right margin
   * @return true if opening and initialization are ok, false otherwise.
   */
  bool open(const string &filename,
	    const string &generator,
	    int w, int h=NB_IMAGES, int days=NB_DAYS,
	    int mup=MARGIN_UP,
	    int mbotttom=MARGIN_BOTTOM,
	    int mleft=MARGIN_LEFT,
	    int mright=MARGIN_RIGHT);
  /**
   *
   */
  void close();

  /**
   * Return the size of the margin which name is provided as parameter to the function.
   * @param m the name of the margin
   * @return the size of the corresponding margin
   */
  int margin(Margin m);

  /**
   * Draws the background of the SVG image according to the specified color.
   * @param color the name of the background color - recognized names are 
   * grey (default), red, green, orange, yellow, blue, violet and white.
   */
  void background(const string &color);
  
   /**
   * Draws a text according to the SVG format.
   * @param x the x-axis location of the center of the text
   * @param y the y-axis location of the center of the text
   * @param size the font size of the text
   * @param txt the text
   * @param color the name of the text color 
   */
  void text(int x, int y, int size, const string &txt, const string &color="black");
  
   /**
   * Draws a line between two points in the SVG format.
   * @param x1 the x-axis location of the first point
   * @param y1 the y-axis location of the first point
   * @param x2 the x-axis location of the second point
   * @param y2 the y-axis location of the second point
   * @param color  the name of the line color 
  */
  void line(int x1, int y1, int x2, int y2, const string &color="black");
  
   /**
   * Draws a rectangles in the SVG format.
   * @param width the width of the rectangle
   * @param height the height of the rectangle
   * @param x the x-axis location of the upper left corner of the rectangle
   * @param y the y-axis location of the  upper left corner of the rectangle
   * @param color the name of the rectangle color 
  */
  void rect(int width, int height, int x, int y, const string &color="black");

  
   /**
   * Draws a circle  in the SVG format.
   * @param xc the x-axis location of the center of the circle
   * @param yc the y-axis location of the center of the circle
   * @param r the radius of the circle
   * @param color the name of the circle color 
  */
  void circle(int xc, int yc, int r, const string &color="black");

   /**
   * Draws a symbol "//" which represents the disappearance of a cell during the experiment
   * @param xc the x-axis location of the center of the symbol
   * @param yc the y-axis location of the center of the symbol
   * @param size the size of the symbol
   * @param color the name of the symbol color 
   */  
  void parallel(int xc, int yc, int size, const string &color="black");

   /**
   * Draws a symbol "X" which represents the death of a cell during the experiment
   * @param xc the x-axis location of the center of the symbol
   * @param yc the y-axis location of the center of the symbol
   * @param size the size of the symbol
   * @param color the name of the symbol color 
   */    
  void cross(int xc, int yc, int size, const string &color="black");

};

#endif
