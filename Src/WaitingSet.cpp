/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "WaitingSet.hpp"

WaitingElement::WaitingElement(){
  pcell = nullptr;
  nbParentsEncountered = 0;
}

WaitingElement::WaitingElement(Node *pcell, int b, int w, int t){
  this->pcell = pcell;
  nbParentsEncountered = 0;
  location loc={b, w, t};
  pos.push_back(loc);
}


int WaitingSet::add(WaitingElement e){
  int id=-1;
  // we check if the element already exists
  for(int i=0; i<elt.size(); i++)
    if(e.pcell == elt[i].pcell){// the element exists
      elt[i].nbParentsEncountered++;
      elt[i].pos.push_back(e.pos[0]);
      id = i;
      break;
    }

  if(id==-1){// the element does not exist or the list is empty
      elt.push_back(e);
      elt[elt.size()-1].nbParentsEncountered++;
      return -1;// the element cannot be ready because it has only one parent
  }

  // the element exists, we test if we can display it
  if(elt[id].isready()) return id;
  return -1;
 
}

ostream& operator<<(ostream& out, const WaitingElement &e){
  out << "Waiting Element :" << endl;
  out << "Cell : " << *(e.pcell);
  out << " nbpe = " << e.nbParentsEncountered << endl;
  return out;
}


void WaitingElement::tosvg(SvgExporter &exporter, int radius){
  // calculation of the central position in relation to the parents
  int xmin=100000, xmax=0;
  for(int i=0; i<pos.size(); i++){
    int p = pos[i].beg+pos[i].width/2;
    if(p<xmin) xmin = p;
    if(p>xmax) xmax = p;
  }
  int x = xmin + (xmax-xmin)/2;
  int widthFusion = xmax-xmin;

  // calculation of the lifetimes for the merged cells
  int tmin=100000, tmax=0;
  for(int i=0; i<pos.size(); i++){
    int t = pcell->getStart()-pos[i].t;
    if(t>tmax) tmax = t;
    if(t<tmin) tmin = t;
  }
  int tdiag = tmin/4;// height of the diagonal link
  
  // draw connections from parent cells
  #define V113
  #ifdef V113
  for(int i=0; i<pos.size(); i++){
    exporter.line(pos[i].beg+pos[i].width/2,
		  pos[i].t+exporter.margin(UP)+radius,
		  pos[i].beg+pos[i].width/2,
		  pcell->getStart()+exporter.margin(UP)-tdiag, "black");
    exporter.line(pos[i].beg+pos[i].width/2,
		  pcell->getStart()+exporter.margin(UP)-tdiag,
		  x, pcell->getStart()+exporter.margin(UP), "black");
  }
  exporter.text(x, pcell->getStart()+exporter.margin(UP)-3*radius ,
		2*radius, to_string(pcell->getStart()));
  
  // resume display from the merged cell
  pcell->tosvg(exporter, x-widthFusion/2, widthFusion);
  #else
    for(int i=0; i<pos.size(); i++){
    exporter.line(pos[i].beg+pos[i].width/2,
		  pos[i].t+radius,
		  pos[i].beg+pos[i].width/2,
		  pcell->getStart()-tdiag, "black");
    exporter.line(pos[i].beg+pos[i].width/2,
		  pcell->getStart()-tdiag,
		  x, pcell->getStart(), "black");
  }
  // exporter.text(x, pcell->getStart()-radius ,
  // 		2*radius, to_string(pcell->getStart()));
  exporter.text(x+3*radius, pcell->getStart()+2*radius ,
		2*radius, to_string(pcell->getStart()));
    // reprendre l'affichage à partir de la cellule fusionnée
  pcell->tosvg(exporter, x);//-widthFusion/2);
#endif



}
