/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SvgExporter.hpp"
#include <sstream>
#include <ctime>
using namespace std;

SvgExporter::SvgExporter(){
}


SvgExporter::~SvgExporter(){
  if(out.is_open()) close();
}


bool SvgExporter::open(const string &filename, const string &generator,
		       int w, int im, int days,
		       int mu, int mb, int ml, int mr){
  out.open(filename);
  if(!out.is_open()){
    cerr << "Impossible to create SVG output file "<< filename << endl;
    return false;
  }

  // initialization of page attributes
  pageWidth = w+ml+mr;
  pageHeight = im+mu+mb;

  mup = mu; mbottom = mb; mleft = ml; mright = mr;
  nbDays = days; nbImages = im;
  
  // header output
  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
  out << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" ";
  out <<"width=\"" << pageWidth << "\" height=\"" << pageHeight << "\">" << endl;

  // sortie de la description
  std::time_t result = std::time(nullptr);
  out << "<desc> Generated with "<< generator << " on " << std::asctime(std::localtime(&result)) << "</desc>" << endl;
  
  return true;
}

void SvgExporter::close(){
  if(out.is_open()){
    out << "</svg>" << endl;
    out.close();
  }
}


int SvgExporter::margin(Margin m){
  switch(m){
  case UP: return mup; break;
  case BOTTOM: return mbottom;
  case LEFT: return mleft; break;
  case RIGHT: return mright; break;
  }
  return 0;
}
      


void SvgExporter::background(const string &color){
  // margins drawing
  rect(pageWidth,mup,0, 0, "white");
  rect(pageWidth,mbottom,0,pageHeight-mbottom, "white");
  rect(mleft, pageHeight, 0, 0, "white");
  rect(mright,pageHeight, pageWidth-mright,0, "white");

  string colorlight, colordark;
  if(color=="grey"){
    colorlight = "rgb(230,230,230)";
    colordark = "rgb(192,192,192)";
  } else if(color=="red"){
    colorlight =  "mistyrose";
    colordark = "lightpink";
  } else if(color=="green"){
    colorlight = "palegreen";
    colordark = "mediumseagreen";
  } else if(color=="orange"){    
    colorlight = "peachpuff";
    colordark = "darksalmon";
  } else if(color=="yellow"){
    colorlight = "lemonchiffon";
    colordark = "wheat";
  } else if(color=="blue"){
    colorlight = "lightcyan";
    colordark = "skyblue";
  } else if(color=="violet"){   
     colorlight = "lavenderblush";
    colordark = "lavender";
  } else if(color=="white"){   
    colorlight = "snow" ;
    colordark = "ghostwhite";
  }else{
    cerr << "color " << color << " unknown - grey assumed ..." << endl;
    colorlight = "rgb(230,230,230)";
    colordark = "rgb(192,192,192)";
  }
  // division of the cells area into "days" vertical zones
  for(int i=0; i<nbDays; i++){
    string color=(i%2) ?   colorlight  : colordark ;
    rect(pageWidth-mup-mright, (pageHeight-mup-mbottom)/5,
	 mleft, mup+ i*(pageHeight-mup-mbottom)/5, color);
  }

  int txtSize=10;

  // drawing days
  for(int i=0; i<nbDays; i++){
    stringstream str;
    str << "D" << i;
    text(mleft/2, mup+ (i+0.5)*(pageHeight-mup-mbottom)/5, 10, str.str(), "red");
  }
    
  // drawing of the scale
  for(int i=0; i<=nbDays; i++){
    stringstream str;
    str <<  i*NB_IMAGES/NB_DAYS;
    text(mleft/2, mup+ i*(pageHeight-mup-mbottom)/5+txtSize/2, txtSize, str.str(), "black");
  }
 
}


void SvgExporter::text(int x, int y, int size, const string &txt, const string &color){

  out << "<text x=\"" << x << "\" y=\"" << y << "\"" ;
  out << " font-family=\"Verdana\" font-size=\"" << size << "\"";
  out << " text-anchor=\"middle\" " ;
  out << " dominant-baseline=\"middle\" ";
  out << "fill=\"" << color << "\" > ";
  out << txt << "</text>" << endl;
}


void SvgExporter::line(int x1, int y1, int x2, int y2, const string &color){
  out << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\"";
  out << " x2=\"" << x2 << "\" y2=\"" << y2 << "\" stroke=\"" << color << "\"";
  out << " />" << endl;
}


void SvgExporter::rect(int width, int height, int x, int y, const string &color){
  out << "<rect width=\"" << width << "\" height=\"" << height << "\"";
  out << " x=\"" << x << "\" y=\"" << y << "\" fill=\"" << color << "\"";
  out << " />" << endl;
}

void SvgExporter::circle(int xc, int yc, int r, const string &color){
  out << "<circle cx=\"" << xc << "\" cy=\"" << yc << "\"";
  out << " r=\"" << r << "\" fill=\"" << color << "\" stroke=\"black\"";
  out << " />" << endl;
}

void SvgExporter::cross(int xc, int yc, int size, const string &color){
  line(xc-size/2, yc-size/2, xc+size/2, yc+size/2, color);
  line(xc-size/2, yc+size/2, xc+size/2, yc-size/2, color);
}

void SvgExporter::parallel(int xc, int yc, int size, const string &color){
  line(xc-size/2, yc+size/2, xc, yc-size/2, color);
  line(xc, yc+size/2, xc+size/2, yc-size/2, color);
}

