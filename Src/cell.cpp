/*
  This file is part of cell, a software package that schematically represents
  the evolution of a cell population after a video-microscopy experiment and
  provides statistical data on this evolution.

  Copyright (c) 2021-2022 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include "Node.hpp"
#include "Input.hpp"
#include "SvgExporter.hpp"
#include "Stat.hpp"

/**
 * toto-cell is a software that allows you to read back data
 * from a cell evolution sequence analysis and produce a graph in SVG format. 
 */

#define WIDTH_CELL  50 /**< width in pixels for the display of each terminal cell */
#define PAGE_HEIGHT 500 /**< height of the image in number of pixels */

#define VERSION "1.3"
// Updated from version 1.1.3 by correcting some bad tree drawings
// date : July 25th 2022


// Global variables
vector <Name> names; /**< dictionary of cell names */
vector <Node *> cells;/**< list of cell trees */
string color; /**< Display color */

// pre-defining functions
void print(const string &filename, const string &generator);
void copyright();

/**
 * main function. 
 * @param argc number of arguments of the command (must be 2)
 * @param argv list of arguments of the commands. 
 * The syntax of the command must be : <commandName>  <event file name>
 */
int main(int argc, char *argv[]){
  // printing the copyright
  copyright();
  
  //verifying parameters
  if(argc != 2){
    cerr << "Syntax = " << argv[0] << " <event file name>" << endl;;
    return 0;
  }
  
  // generate output file names from event file name
  // the two output files are :
  // - the SVG image
  // - the statistics file (scv format)
  string evtfilename(argv[1]);
  string svgfilename, csvfilename;
  
  int pos = evtfilename.find_last_of('.');
  if(pos== string::npos){// no extension
    svgfilename = evtfilename;
    csvfilename = evtfilename;
  }else{
    svgfilename = evtfilename.substr(0,pos);
    csvfilename = evtfilename.substr(0,pos);
  }

  svgfilename.append(".svg");
  csvfilename.append(".csv");

  // initalizing log file name
  string logfilename = "out.log";



  // initialize default color
  color = "grey";

  // loading the cell evolution sequences file
  if(!read(argv[1], logfilename)) return 0;

  // computation of statistics
  Statistics stat; 

  for(int i=0; i<cells.size(); i++){
    bool divActive=false; // <- à revoir !!!
    cells[i]->collectStatistics(stat, false, -1);
  }
  stat.computeAverages();

  // printing the SVGfile
  stringstream generator;
  string exe(argv[0]);
  size_t found = exe.find_last_of('/');
  if(found!=string::npos) generator << exe.substr(found+1);
  else generator << exe;
  generator << " v" << VERSION;
  
  print(svgfilename, generator.str());

  // printing the CSV file
  stat.toCSV(csvfilename);

  // memory cleaning
  names.clear();
  cells.clear();

  // end message
  cout << "Cell ended correctly" << endl;
  
  return 1;
}

/**
 * Function allowing the graphic output in SVG format of the different
 * trees representing the evolution of the cells.
 * @param filename name of the save file in SVG format
 */
void print(const string &filename, const string &generator){
  
  // recursive calculation of the number of final cells
  int nbcol=0;// nb of final cells
  for(int i=0; i<cells.size(); i++){
    nbcol += cells[i]->width(1);
  }

  // calculation of the width of the image
  int width = nbcol*WIDTH_CELL;

  // --- export to svg format ---
  SvgExporter exporter;

  if(!exporter.open(filename, generator, width, 721)){
    cerr << "error when exporting to svg" << endl;;
    return;
  }
  // setting the background color
  exporter.background(color);


  // printing each cell tree to SVG
  nbcol = 0;
  for(int i=0; i<cells.size(); i++){
    int cwidth = cells[i]->width(1);
    cells[i]->tosvg(exporter, nbcol*WIDTH_CELL+exporter.margin(LEFT), cwidth*WIDTH_CELL );
    nbcol+=cwidth;
  }

  // closing the SVG file
  exporter.close();
}

/**
 * Function that prints the toto cell copyright
 */
void copyright(){
  cout << "Cell V" << VERSION;
  cout << " - Copyright C. RENAUD 2021-2022" << endl;
}
